﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class Product
    {
        [Key]
        public virtual int ProductId { get; set; }

        [StringLength(30, MinimumLength = 1)]
        [Display(Name = "Product Name")]
        public virtual string Title { get; set; }

        [StringLength(300, MinimumLength = 1)]
        [Display(Name = "Product Description")]
        public virtual string Description { get; set; }

        [StringLength(30, MinimumLength = 1)]
        [Display(Name = "Image Link")]
        public virtual string ProductImg { get; set; }

        [Display(Name = "Product Price")]
        public virtual decimal Price { get; set; }

        [Display(Name = "Stock Level")]
        [Range(0, 999)]
        public virtual int Quantity { get; set; }

        [StringLength(20, MinimumLength = 1)]
        [Display(Name = "Product Colour")]
        public virtual string Colour { get; set; }
    }
}
