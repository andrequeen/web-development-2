namespace Shop.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Shop.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Shop.Models.ApplicationDbContext context)
        {

            context.Products.AddOrUpdate(x => x.ProductId,
                            new Models.Product()
                            {
                                ProductId = 1,
                                Title = "Stockholm Charcoal",
                                Description = "Premium quality T-shirt in Organic and Fairtrade certified cotton, with our script logo embroidered on the chest.",
                                Colour = "Charcoal",
                                Price = 16,
                                ProductImg = "stockCharcoal.jpg",
                                Quantity = 8
                            },
                            new Models.Product()
                            {
                                ProductId = 2,
                                Title = "Stockholm White",
                                Description = "Premium quality T-shirt in Organic and Fairtrade certified cotton, with our script logo embroidered on the chest.",
                                Colour = "White",
                                Price = 22,
                                ProductImg = "stockWhite.jpg",
                                Quantity = 14
                            },
                            new Models.Product()
                            {
                                ProductId = 3,
                                Title = "Stockholm Black",
                                Description = "Premium quality T-shirt in Organic and Fairtrade certified cotton, with our script logo embroidered on the chest.",
                                Colour = "Black",
                                Price = 30,
                                ProductImg = "stockBlack.jpg",
                                Quantity = 3
                            },
                            new Models.Product()
                            {
                                ProductId = 4,
                                Title = "Stockholm Grey",
                                Description = "Premium quality T-shirt in Organic and Fairtrade certified cotton, with our script logo embroidered on the chest.",
                                Colour = "Grey",
                                Price = 13,
                                ProductImg = "stockGrey.jpg",
                                Quantity = 8
                            },
                            new Models.Product()
                            {
                                ProductId = 5,
                                Title = "Planet Wookie",
                                Description = "Star Wars t-shirt with Planet Wookie print made from 100% organic cotton. This collection is officially licensed by Star Wars. See all Star Wars products available at Tshirt Store online.",
                                Colour = "Image",
                                Price = 15,
                                ProductImg = "planetWookie.jpg",
                                Quantity = 7
                            },
                            new Models.Product()
                            {
                                ProductId = 6,
                                Title = "R2D2",
                                Description = "Star Wars t-shirt with R2D2 print made from 100% organic cotton. This collection is officially licensed by Star Wars. See all Star Wars products available at Tshirt Store online.",
                                Colour = "Black",
                                Price = 30,
                                ProductImg = "r2d2.jpg",
                                Quantity = 3
                            },
                            new Models.Product()
                            {
                                ProductId = 7,
                                Title = "Vader",
                                Description = "Star Wars t-shirt with Darth Vader print made from 100% organic cotton. This collection is officially licensed by Star Wars. See all Star Wars products available at Tshirt Store online.",
                                Colour = "Image",
                                Price = 30,
                                ProductImg = "vader.jpg",
                                Quantity = 9
                            },
                            new Models.Product()
                            {
                                ProductId = 8,
                                Title = "Pug Life",
                                Description = "100% Fairtrade Certified & Organic Cotton.",
                                Colour = "Image",
                                Price = 23,
                                ProductImg = "pugLife.jpg",
                                Quantity = 19
                            },
                            new Models.Product()
                            {
                                ProductId = 9,
                                Title = "Stick City",
                                Description = "100% Fairtrade Certified & Organic Cotton.",
                                Colour = "Image",
                                Price = 33,
                                ProductImg = "stickCity.jpg",
                                Quantity = 17
                            },
                            new Models.Product()
                            {
                                ProductId = 10,
                                Title = "Oxford Shirt White",
                                Description = "Straight Fit Shirt with a Buttoned Down. 100 % Organic Cotton certified by Fairtrade & GOTS",
                                Colour = "White",
                                Price = 54,
                                ProductImg = "oxfordWhite.jpg",
                                Quantity = 14
                            },
                            new Models.Product()
                            {
                                ProductId = 11,
                                Title = "Oxford Shirt Black",
                                Description = "Straight Fit Shirt with a Buttoned Down. 100 % Organic Cotton certified by Fairtrade & GOTS",
                                Colour = "Black",
                                Price = 34,
                                ProductImg = "oxfordBlack.jpg",
                                Quantity = 18
                            },
                            new Models.Product()
                            {
                                ProductId = 12,
                                Title = "Solo Biker",
                                Description = "Solo Biker print by the talented Eliza Southwood. Printed on our classic Stockholm t-shirt in off-white.",
                                Colour = "Image",
                                Price = 12,
                                ProductImg = "solobiker.jpg",
                                Quantity = 24
                            },
                            new Models.Product()
                            {
                                ProductId = 13,
                                Title = "Kraft Biker",
                                Description = "A classic that we have brought back by popular demand. Illustrated by Nick Alston and printed on our classic Stockholm T-shirt in White. 100% Cotton. Organic and Fairtrade certified as always!",
                                Colour = "Image",
                                Price = 22,
                                ProductImg = "kraftbiker.jpg",
                                Quantity = 53
                            },
                            new Models.Product()
                            {
                                ProductId = 14,
                                Title = "Retro VHS",
                                Description = "Nothing is more retro these days than a stack of VHS Tapes. 100% organic cotton, certified by GOTS and Fairtrade.",
                                Colour = "Image",
                                Price = 26,
                                ProductImg = "retrovhs.jpg",
                                Quantity = 3
                            },
                            new Models.Product()
                            {
                                ProductId = 15,
                                Title = "Star Wars Shirt",
                                Description = "Star Wars Button-down Oxford Shirt with Space Ships print made from 100% organic cotton. This collection is officially licensed by Star Wars.See all Star Wars products available at Tshirt Store online.",
                                Colour = "Image",
                                Price = 22,
                                ProductImg = "starwars.jpg",
                                Quantity = 8
                            },
                            new Models.Product()
                            {
                                ProductId = 16,
                                Title = "Bistro Shirt",
                                Description = "A classic all-over print by legendary swedish graphic designer Olle Eksell printed on our button down oxford shirt.",
                                Colour = "Image",
                                Price = 33,
                                ProductImg = "bistroshirt.jpg",
                                Quantity = 38
                            },
                            new Models.Product()
                            {
                                ProductId = 17,
                                Title = "Dove Shirt",
                                Description = "The fantastic all over doves print on our oxford button down shirt in blue chambray, Chest pocket on left side with logo patch.",
                                Colour = "Image",
                                Price = 8,
                                ProductImg = "doveshirt.jpg",
                                Quantity = 17
                            },
                            new Models.Product()
                            {
                                ProductId = 18,
                                Title = "Bike Shirt",
                                Description = "The instant classic Bike People illustrated by the talented Eliza Southwood on our classic button-down oxford shirt.",
                                Colour = "Image",
                                Price = 60,
                                ProductImg = "bikeshirt.jpg",
                                Quantity = 45
                            },
                            new Models.Product()
                            {
                                ProductId = 19,
                                Title = "Beast",
                                Description = "The timeless classic Beast printed on our classic Stockholm fit in bloody orange.",
                                Colour = "Image red",
                                Price = 7,
                                ProductImg = "beast.jpg",
                                Quantity = 3
                            },
                            new Models.Product()
                            {
                                ProductId = 20,
                                Title = "Chicago Blackhawks",
                                Description = "100% Organic Cotton certified by Fairtrade & GOTS This collection is officially licensed by NHL.See all NHL products available at Tshirt Store online.",
                                Colour = "Image Black",
                                Price = 29,
                                ProductImg = "chicago.jpg",
                                Quantity = 4
                            },
                            new Models.Product()
                            {
                                ProductId = 21,
                                Title = "Yellow stripe polo shirt",
                                Description = "A great looking men's polo shirt, the Wells is manufactured in comfortable heavy open weave pique fabric - which is pre-shrunk for quality and comfort. Details include a button placket, flat knit collar, hem side splits with contrast tape and rib cuffs.",
                                Colour = "Pattern",
                                Price = 19,
                                ProductImg = "polo2.jpg",
                                Quantity = 33
                            },
                            new Models.Product()
                            {
                                ProductId = 22,
                                Title = "Dark stripe polo shirt",
                                Description = "A great looking men's polo shirt, the Wells is manufactured in comfortable heavy open weave pique fabric - which is pre-shrunk for quality and comfort. Details include a button placket, flat knit collar, hem side splits with contrast tape and rib cuffs.",
                                Colour = "Pattern",
                                Price = 21,
                                ProductImg = "polo1.jpg",
                                Quantity = 32
                            },
                            new Models.Product()
                            {
                                ProductId = 23,
                                Title = "Tri colour stripe polo shirt",
                                Description = "A firm Help for Heroes favourite, this striped polo shirt is finished in red, navy blue and light blue with contrasting embroidered branding on the chest and sleeve so you can show your support with pride.",
                                Colour = "Pattern",
                                Price = 21,
                                ProductImg = "polo3.jpg",
                                Quantity = 54
                            },
                            new Models.Product()
                            {
                                ProductId = 24,
                                Title = "Red Polo",
                                Description = "Pick a polo that will deliver cooling, sun-protective performance this summer in plain and striped colourways to suit every mood! Creston is made from moisture-wicking polyester pique, providing day-long comfort when the heat is on.",
                                Colour = "Pattern",
                                Price = 37,
                                ProductImg = "polo4.jpg",
                                Quantity = 64
                            }
                        );


        }
    }
}
